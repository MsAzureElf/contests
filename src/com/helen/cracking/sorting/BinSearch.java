package com.helen.cracking.sorting;

import java.util.Scanner;

/**
 * Created by Helen on 08.01.2018.
 */
public class BinSearch {

    private static int binSearch(int[]array, int l, int r, int n){
        if (l > r)
            return -1;
        int m = (l+r)/2;
        if (array[m] > n)
            return binSearch(array, l, m-1, n);
        else if (array[m] < n)
            return binSearch(array, m+1, r, n);
        return m;
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int []a = new int[N];
        for (int i=0;i<a.length;++i)
            a[i] = i*2;
        int M = cin.nextInt();
        System.out.println(binSearch(a, 0, N-1, M));
    }

}
