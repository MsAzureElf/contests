package com.helen.cracking.ya_algo.qualification;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Lottery {

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        Set<Integer> set = new HashSet<>();
        for (int i=0;i<10;++i)
            set.add(cin.nextInt());
        int N = cin.nextInt();
        for (int i=0;i<N;++i){
            int cnt = 0;
            for (int j=0;j<6;++j){
                if (set.contains(cin.nextInt()))
                    cnt++;
            }
            System.out.println((cnt<3)?"Unlucky":"Lucky");
        }
    }

}
