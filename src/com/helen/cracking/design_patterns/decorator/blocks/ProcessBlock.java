package com.helen.cracking.design_patterns.decorator.blocks;

import com.helen.cracking.design_patterns.decorator.AbstractBlock;

public class ProcessBlock implements AbstractBlock {

    @Override
    public void draw() {
        System.out.println("ProcessBlock drawing...");
    }
}
