package com.helen.cracking.arrays_strings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Helen on 02.01.2018.
 */
public class SynchronizedList {

    private static List list = Collections.synchronizedList(new ArrayList<Integer>());

    public static void main(String[] args) {
        synchronized (list){
            Iterator iterator = list.iterator();
            while (iterator.hasNext()){
                System.out.println(iterator.next());
            }
        }
    }

}
