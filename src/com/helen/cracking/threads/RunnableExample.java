package com.helen.cracking.threads;

public class RunnableExample implements Runnable {

    private static final int COUNT_MAX = 5;
    private int count = 0;

    @Override
    public void run() {
        System.out.println("run() started");
        try {
            while (count < COUNT_MAX){
                count += 1;
                System.out.println("count = " + count);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e){
            System.out.println("run() interrupted");
        }
        System.out.println("run() finished");
    }

    public int getCount() {
        return count;
    }

    public static void main(String[] args) {
        RunnableExample runnable = new RunnableExample();
        Thread thread = new Thread(runnable);
        thread.run();
        while (runnable.getCount() < COUNT_MAX){
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
