package com.helen.cracking.design_patterns.parking_lot.vehicles;

import com.helen.cracking.design_patterns.parking_lot.ParkingSpot;
import com.helen.cracking.design_patterns.parking_lot.Vehicle;

/**
 * Created by Helen on 03.01.2018.
 */
public class Car extends Vehicle {

    public Car(){
        this.size = VehicleSize.Small;
        this.spotsOccupied = 1;
    }

    @Override
    public boolean fitsInSpot(ParkingSpot spot) {
        return false;
    }
}
