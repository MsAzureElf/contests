package com.helen.cracking.stacks_queues;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * Created by Helen on 07.01.2018.
 */
public class GetMedian {

    private static PriorityQueue<Integer> minQueue = new PriorityQueue<Integer>(100, new Comparator<Integer>() {
        @Override
        public int compare(Integer i1, Integer i2) {
            if (i1 < i2)
                return 1;
            if (i1 > i2)
                return -1;
            return 0;
        }
    }),
    maxQueue = new PriorityQueue<Integer>(100, new Comparator<Integer>() {
        @Override
        public int compare(Integer i1, Integer i2) {
            if (i1 > i2)
                return 1;
            if (i1 < i2)
                return -1;
            return 0;
        }
    });

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        while (true){
            String s = cin.nextLine();
            if (s.compareTo("?") == 0){
                if (minQueue.size() == maxQueue.size())
                    System.out.println((minQueue.peek()+maxQueue.peek())/2);
                else if (minQueue.size() > maxQueue.size())
                    System.out.println(minQueue.peek());
                else
                    System.out.println(maxQueue.peek());
            } else {
                int value = Integer.parseInt(s);
                if (maxQueue.size() > 0 && value > maxQueue.peek())
                    maxQueue.add(value);
                else
                    minQueue.add(value);
                if (minQueue.size() > maxQueue.size()+1)
                    maxQueue.add(minQueue.poll());
                if (maxQueue.size() > minQueue.size()+1)
                    minQueue.add(maxQueue.poll());
            }
        }
    }

}
