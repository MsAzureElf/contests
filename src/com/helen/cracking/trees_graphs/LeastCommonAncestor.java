package com.helen.cracking.trees_graphs;

import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Helen on 28.01.2018.
 */
public class LeastCommonAncestor {

    private static class Node {

        public int value;
        public Node left, right;

        public Node(int value){
            this.value = value;
            left = null;
            right = null;
        }

    }

    private static HashMap<Integer, HashMap<Integer, Node>> searchMap = new HashMap<>();

    public static Node search(Node node, int value){
        if (node == null)
            return null;
        if (searchMap.containsKey(node.value)
                && searchMap.get(node.value).containsKey(value))
            return searchMap.get(node.value).get(value);
        if (node.value == value) {
            if (!searchMap.containsKey(node.value))
                searchMap.put(node.value, new HashMap<>());
            searchMap.get(node.value).put(value, node);
            return node;
        }
        Node left = search(node.left, value);
        if (left != null)
            return left;
        return search(node.right, value);
    }

    public static int LCA(Node node, int value1, int value2){
        if (node == null)
            return -1;
        if (value1 == node.value && search(node, value2) != null)
            return node.value;
        if (value2 == node.value && search(node, value1) != null)
            return node.value;
        Node leftV1 = search(node.left, value1), rightV1 = search(node.right, value1);
        Node leftV2 = search(node.left, value2), rightV2 = search(node.right, value2);
        if (leftV1 != null && rightV2 != null || leftV2 != null && rightV1 !=null)
            return node.value;
        if (leftV1 != null && leftV2 != null)
            return LCA(node.left, value1, value2);
        if (rightV1 != null && rightV2 != null)
            return LCA(node.right, value1, value2);
        return -1; //not found
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        Node root = new Node(cin.nextInt());
        for (int i=1;i<N;++i){
            int parent = cin.nextInt(), value = cin.nextInt();
            Node parentNode = search(root, parent);
            if (parentNode != null) {
                if (parentNode.left == null)
                    parentNode.left = new Node(value);
                else parentNode.right = new Node(value);
            }
        }
        System.out.println(LCA(root, cin.nextInt(), cin.nextInt()));
    }

}
