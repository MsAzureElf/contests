package com.helen.cracking.trees_graphs;

import java.util.Scanner;

/**
 * Created by Helen on 03.02.2018.
 */
public class FindRotatingPoint {

    private static int search(int[] a, int l, int r) {
        if (r < l)
            return -1;
        if (a[l] <= a[r])
            return l;
        int m = (l + r) / 2;
        if (a[l] <= a[m])
            return search(a, m + 1, r);
        return search(a, l, m);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int[] a = new int[N];
        for (int i = 0; i < N; ++i)
            a[i] = cin.nextInt();
        System.out.println(search(a, 0, N - 1));
    }

}
