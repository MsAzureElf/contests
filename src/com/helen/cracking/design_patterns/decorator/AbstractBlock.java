package com.helen.cracking.design_patterns.decorator;

public interface AbstractBlock {

    void draw();
}
