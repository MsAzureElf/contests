package com.helen.cracking.eolymp;

import java.util.Scanner;

public class Discriminar {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] mass = new int[3];
        for (int i = 0; i < mass.length; i++) {
            mass[i] = scanner.nextInt();
        }
        double d = mass[1] * mass[1] - 4 * mass[0] * mass[2];

        if (d > 0) {
            int x, y;
            x = (int) ((-mass[1] + Math.sqrt(d)) / (2 * mass[0]));
            y = (int) ((-mass[1] - Math.sqrt(d)) / (2 * mass[0]));
            if (x < y)
                System.out.println("Two roots: " + x + " " + y);
            else System.out.println("Two roots: " + y + " " + x);
        }
        else if (d == 0){
            int x = -mass[1] / (2* mass[0]);
            System.out.println("One root: " + x);
        }
        else System.out.println("No roots");
    }
}

