package com.helen.cracking.design_patterns.parking_lot;

import java.util.ArrayList;

/**
 * Created by Helen on 03.01.2018.
 */
public abstract class Vehicle {

    public enum VehicleSize {Motorcycle, Small, Large};
    protected ArrayList<ArrayList<ParkingSpot>> parkingSpots = new ArrayList<>();


    protected int spotsOccupied;
    protected VehicleSize size;

    public int getSpotsOccupied() {
        return spotsOccupied;
    }

    public VehicleSize getSize() {
        return size;
    }

    public void parkInSpot(){

    }

    public void clearSpot(){

    }

    public abstract boolean fitsInSpot(ParkingSpot spot);

}
