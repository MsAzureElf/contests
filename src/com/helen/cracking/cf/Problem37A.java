package com.helen.cracking.cf;

import java.util.Scanner;

/**
 * Created by Helen on 07.02.2018.
 */
public class Problem37A {

    public static void main(String [] args){
        Scanner cin = new Scanner(System.in);
        int T = cin.nextInt();
        for (int t=0;t<T;++t){
            int N = cin.nextInt(), k = cin.nextInt();
            int time = 0;
            int []a = new int[k];
            for (int i=0;i<k;++i)
                a[i] = cin.nextInt()-1;
            for (int i=0;i<N;++i) {
                int minTime = Integer.MAX_VALUE;
                for (int j = 0; j < k; ++j)
                    minTime = Math.min(minTime, Math.abs(a[j]-i)+1);
                time = Math.max(time, minTime);
            }
            System.out.println(time);
        }
    }

}
