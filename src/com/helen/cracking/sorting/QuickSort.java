package com.helen.cracking.sorting;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Helen on 08.01.2018.
 */
public class QuickSort {

    private static int partition(int[]array, int l, int r){
        int m = array[(l+r)/2];
        int l_init = l, r_init = r;
        while (l <= r) {
            while (array[l] <= m && l < r_init)
                l++;
            while (m <= array[r] && r > l_init)
                r--;
            int t = array[l];
            array[l] = array[r];
            array[r] = t;
            l++;
            r--;
        }
        return l;
    }

    private static void sort(int[] array, int l, int r) {
        if (l >= r)
            return;
        int m = partition(array, l, r);
        sort(array, l, m-1);
        sort(array, m, r);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int[] a = new int[N];
        for (int i = 0; i < a.length; ++i)
            a[i] = cin.nextInt();
        sort(a, 0, N - 1);
        for (int i = 0; i < N; ++i)
            System.out.print(a[i] + " ");
    }

}
