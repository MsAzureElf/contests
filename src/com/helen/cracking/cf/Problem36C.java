package com.helen.cracking.cf;

import java.util.*;

/**
 * Created by Helen on 05.02.2018.
 */
public class Problem36C {

    private static char[] result = null;

    private static boolean getMaxNotExceeding(int[] a, char[] b, int ind) {
        if (ind >= b.length)
            return true;
        int k = b[ind] - '0';
        boolean res = false;
        if (a[k] > 0) {
            a[k]--;
            res = getMaxNotExceeding(a, b, ind + 1);
            a[k]++;
        }
        if (res) {
            result[ind] = b[ind];
            return true;
        }
        k--;
        while (k >= 0) {
            if (a[k] > 0)
                break;
            k--;
        }
        if (k < 0)
            return false;
        a[k]--;
        result[ind] = (char) (k + '0');
        k = 1;
        for (int i = a.length - 1; i >= 0; --i) {
            for (int j = 1; j <= a[i]; ++j) {
                result[ind + k] = (char) (i + '0');
                k++;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        char[] a = cin.next().toCharArray();
        char[] b = cin.next().toCharArray();
        if (a.length < b.length) {
            Arrays.sort(a);
            for (int i = a.length - 1; i >= 0; --i)
                System.out.print(a[i]);
            System.out.println();
            return;
        }
        int[] indexA = new int[10];
        for (int i = 0; i < a.length; ++i)
            indexA[a[i] - '0']++;
        result = new char[a.length];
        getMaxNotExceeding(indexA, b, 0);
        System.out.println(new String(result));
    }

}
