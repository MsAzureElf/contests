package com.helen.cracking.arrays_strings;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Helen on 27.12.2017.
 */
public class UniqueCharacters {

    private static int STRING_LIMIT = (int) Math.pow(2, 16);

    public boolean hasAllUniqueCharacters(String s) {
        boolean[] map = new boolean[STRING_LIMIT];
        Arrays.fill(map, false);
        for (int i = 0; i < s.length(); ++i) {
            int index = s.charAt(i);
            if (!map[index])
                map[index] = true;
            else
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        System.out.println(new UniqueCharacters().hasAllUniqueCharacters(s));
    }

}
