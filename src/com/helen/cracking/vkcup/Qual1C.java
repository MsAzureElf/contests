package com.helen.cracking.vkcup;

import java.util.*;

public class Qual1C {

    private static class Project implements Comparable<Project> {

        public String name;
        public int version;
        public ArrayList<Project> dependencies = new ArrayList<>();
        public Project(String name, int version){
            this.name = name;
            this.version = version;
        }

        @Override
        public int compareTo(Project project) {
            if (this.name.compareTo(project.name) != 0)
                return this.name.compareTo(project.name);
            return Integer.compare(this.version, project.version);
        }
    }


    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        Project []p = new Project[N];
        for (int i=0;i<N;++i){
            p[i] = new Project(cin.next(), cin.nextInt());
            int M = cin.nextInt();
            for (int j=0;j<M;++j)
                p[i].dependencies.add(new Project(cin.next(), cin.nextInt()));
        }
        HashMap<String, Project> mapFull = new HashMap<>();
        for (int i=0;i<N;++i){
            mapFull.put(p[i].name+p[i].version, p[i]);
        }
        for (int i=0;i<N;++i){
            for (int j=0;j<p[i].dependencies.size();++j){
                p[i].dependencies.set(j, mapFull.get(p[i].dependencies.get(j).name+p[i].dependencies.get(j).version));
            }
        }
        HashMap<String, Integer> selected = new HashMap<>();
        selected.put(p[0].name, p[0].version);
        Queue<Project> queue = new LinkedList<>();
        queue.add(p[0]);
        HashMap<String, Integer> currentHeight = new HashMap<>();
        while (!queue.isEmpty()) {
            currentHeight.clear();
            while (!queue.isEmpty()) {
                Project cur = queue.poll();
                for (int i = 0; i < cur.dependencies.size(); ++i) {
                    Project curr = cur.dependencies.get(i);
                    if (!selected.containsKey(curr.name) && (!currentHeight.containsKey(curr.name) || currentHeight.get(curr.name) < curr.version)) {
                        currentHeight.put(curr.name, curr.version);
                    }
                }
            }
            selected.putAll(currentHeight);
            for (String name : currentHeight.keySet()){
                queue.add(mapFull.get(name+currentHeight.get(name)));
            }
        }
        selected.remove(p[0].name);
        List<String> results = new ArrayList<>();
        for (Map.Entry<String, Integer> entry: selected.entrySet()){
            results.add(entry.getKey()+" "+entry.getValue());
        }
        Collections.sort(results);
        System.out.println(results.size());
        for (String result : results) System.out.println(result);
    }

}
