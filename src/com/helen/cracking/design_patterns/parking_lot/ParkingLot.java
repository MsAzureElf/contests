package com.helen.cracking.design_patterns.parking_lot;

import java.util.ArrayList;

/**
 * Created by Helen on 03.01.2018.
 */
public class ParkingLot {

    public static int NUM_LEVELS = 3;

    private ParkingLevel[] levels = new ParkingLevel[NUM_LEVELS];

    public ParkingLot(){
        for (int i=0;i<NUM_LEVELS;++i)
            levels[i] = new ParkingLevel();
    }

    public boolean parkVehicle(Vehicle vehicle){
        for (ParkingLevel level : levels){
            if (level.parkVehicle(vehicle))
                return true;
        }
        return false;
    }

    public ParkingLevel getParkingLevel(int level){
        if (level < 0 || level >= NUM_LEVELS)
            return null;
        return levels[level];
    }

    public int getLevelsCount(){
        return NUM_LEVELS;
    }


}
