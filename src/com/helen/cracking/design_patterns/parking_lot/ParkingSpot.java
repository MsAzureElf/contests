package com.helen.cracking.design_patterns.parking_lot;

/**
 * Created by Helen on 03.01.2018.
 */
public class ParkingSpot {

    private Vehicle parkedVehicle = null;
    private Vehicle.VehicleSize spotSize;

    public ParkingSpot(Vehicle.VehicleSize size){
        this.spotSize = size;
    }

    public boolean isFree(){
        return parkedVehicle == null;
    }

    public boolean fitsInSpot(Vehicle vehicle){
        return false;
    }

    public void parkVehicle(Vehicle vehicle){
        this.parkedVehicle = vehicle;
    }

    public Vehicle removeVehicle(){
        Vehicle vehicle = this.parkedVehicle;
        this.parkedVehicle = null;
        return vehicle;
    }

}
