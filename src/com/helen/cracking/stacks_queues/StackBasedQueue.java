package com.helen.cracking.stacks_queues;

import java.util.ListIterator;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by Helen on 01.01.2018.
 */
public class StackBasedQueue {

    private Stack<Integer> stack = new Stack<Integer>();
    private Stack<Integer> auxStack = new Stack<Integer>();

    public void queue(Integer item){
        stack.push(item);
    }

    public Integer dequeue(){
        while (!stack.isEmpty()){
            auxStack.push(stack.pop());
        }
        Integer item = auxStack.pop();
        while (!auxStack.isEmpty()){
            stack.push(auxStack.pop());
        }
        return item;
    }

    public boolean isEmpty(){
        return stack.isEmpty();
    }

    public void printQueue(boolean printSpace) {
        if (stack.isEmpty()) {
            System.out.println("e");
            return;
        }
        for (ListIterator iterator = stack.listIterator(); iterator.hasNext(); ) {
            System.out.print(iterator.next() + (printSpace ? " " : ""));
        }
        System.out.println();
    }

    public Integer peek(){
        while (!stack.isEmpty()){
            auxStack.push(stack.pop());
        }
        Integer item = auxStack.peek();
        while (!auxStack.isEmpty()){
            stack.push(auxStack.pop());
        }
        return item;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        StackBasedQueue queue = new StackBasedQueue();
        for (int i = 0; i < N; ++i) {
            String s = scanner.nextLine();
            if (s.length() == 0) {
                i--;
                continue;
            }
            if (s.charAt(0) == '-') {
                System.out.println("Pop: " + queue.dequeue());
            } else if (s.charAt(0) == 'p') {
                queue.printQueue(true);
            }
            else {
                queue.queue(Integer.parseInt(s));
            }
        }
    }


}
