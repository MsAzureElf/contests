package com.helen.cracking.sorting;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * Created by Helen on 27.01.2018.
 */
public class MaxHeapGeneric<T extends Comparable> {

    private List<T> heap;
    private int size;

    public MaxHeapGeneric(List<T> array){
        size = array.size();
        heap = array;
        for (int i=size/2-1;i>=0;--i)
            heapify(i);
    }

    private void heapify(int i){
        int largest = i;
        int li = 2*i+1, ri = 2*i+2;
        if (li < size && heap.get(largest).compareTo(heap.get(li)) < 0){
            largest = li;
        }
        if (ri < size && heap.get(largest).compareTo(heap.get(ri)) < 0){
            largest = ri;
        }
        if (largest != i) {
            swap(i, largest);
            heapify(largest);
        }
    }

    private void swap(int i, int j){
        T temp = heap.get(i);
        heap.set(i, heap.get(j));
        heap.set(j, temp);
    }

    private void up(int i){
        if (i == 0)
            return;
        int parent = (i-1)/2;
        if (heap.get(parent).compareTo(heap.get(i)) < 0){
            swap(i, parent);
            up(parent);
        }
    }

    private void add(T item){
        heap.add(item);
        up(size);
        size++;
    }

    public T getMax(){
        if (heap == null || heap.size() == 0)
            return null;
        return heap.get(0);
    }

    public void replaceMax(T value){
        heap.set(0, value);
        heapify(0);
    }

    public void print(){
        for (int i=0;i<size;++i){
            System.out.print(heap.get(i).toString()+" ");
        }
        System.out.println();
    }

    public static class Pair implements Comparable<Pair> {

        public long x, y, r;

        public Pair(int x, int y){
            this.x = x;
            this.y = y;
            r = x*x + y*y;
        }

        public int compareTo(Pair p){
            if (this.r < p.r)
                return -1;
            if (this.r > p.r)
                return 1;
            return 0;
        }

        public String toString(){
            return "(" + x + ", " + y + ")";
        }

    }

    public static void main(String[] args){
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt(), K = cin.nextInt();
        K = Math.min(N, K);
        int x, y;
        ArrayList<Pair> array = new ArrayList<>();
        for (int i=0;i<N;++i){
            x = cin.nextInt();
            y = cin.nextInt();
            array.add(new Pair(x, y));
        }
        MaxHeapGeneric<Pair> heap = new MaxHeapGeneric<>(array.subList(0, K));
        //PriorityQueue<Pair> heap = new PriorityQueue<>(array.subList(0, K));
        //print(heap);
        heap.print();
        for (int i=K;i<N;++i){
            if (heap.getMax().compareTo(array.get(i)) > 0) {
                heap.replaceMax(array.get(i));
                //heap.replaceMax(array.get(i));
            }
        }
        heap.print();
        //print(heap);
    }

    public static void print(PriorityQueue<Pair> queue){
        for (Pair p : queue){
            System.out.print(p.toString()+" ");
        }
        System.out.println();
    }

}
