package com.helen.cracking.design_patterns.decorator;

import com.helen.cracking.design_patterns.decorator.blocks.ProcessBlock;
import com.helen.cracking.design_patterns.decorator.blocks.TerminatorBlock;
import com.helen.cracking.design_patterns.decorator.decorators.BorderBlockDecorator;
import com.helen.cracking.design_patterns.decorator.decorators.LabelBlockDecorator;

public class DecoratorDemo {

    public static void main(String[] args) {

        AbstractBlock processBlock = new ProcessBlock();
        AbstractBlock borderDecoratedProcessBlock = new BorderBlockDecorator(new ProcessBlock());
        AbstractBlock labelDecoratedTerminatorBlock = new LabelBlockDecorator(new TerminatorBlock());

        processBlock.draw();
        borderDecoratedProcessBlock.draw();
        labelDecoratedTerminatorBlock.draw();

    }

}
