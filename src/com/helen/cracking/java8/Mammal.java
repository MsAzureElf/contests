package com.helen.cracking.java8;

public interface Mammal {

    default public String getName(){
        return "NoName";
    }

    public int getAge();

}
