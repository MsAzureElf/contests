package com.helen.cracking.arrays_strings;

/**
 * Created by Helen on 07.01.2018.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class LookAndSay {

    private static class Pair{

        private int item, count;
        public Pair(int item){
            this.item = item;
            this.count = 1;
        }
        public void incCount(){
            this.count++;
        }
        public int getItem(){
            return item;
        }
        public int getCount(){
            return count;
        }

    }

    public static String lookAndSay(String s){
        if (s == null || s.length() == 0)
            return s;
        char []a = s.toCharArray();
        ArrayList<Pair>	list = new ArrayList<Pair>();
        list.add(new Pair(a[0]-'0'));
        for (int i=1;i<a.length;++i){
            if (list.get(list.size()-1).getItem() == a[i]-'0')
                list.get(list.size()-1).incCount();
            else list.add(new Pair(a[i]-'0'));
        }
        StringBuilder builder = new StringBuilder();
        for (int i=0;i<list.size();++i){
            builder.append(list.get(i).getCount()+""+list.get(i).getItem());
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt(), i=0;
        String s = "1";
        while (i < N){
            System.out.println(s=lookAndSay(s));
            i++;
        }
    }
}
