package com.helen.cracking.sorting;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Helen on 08.01.2018.
 */
public class MergeSort {

    private static void merge(int []array, int l, int m, int r){
        int []b = new int[r-l+1];
        int i=l, j=m+1, k=0;
        while (i <= m && j <= r){
            if (array[i] < array[j]){
                b[k] = array[i];
                i++;
            } else {
                b[k] = array[j];
                j++;
            }
            k++;
        }
        while (i <= m){
            b[k++] = array[i++];
        }
        while (j <= r){
            b[k++] = array[j++];
        }
        for (i=0;i<b.length;++i)
            array[i+l] = b[i];
    }

    private static void sort(int[] array, int l, int r) {
        if (l >= r)
            return;
        int m = (l+r)/2;
        sort(array, l, m);
        sort(array, m+1, r);
        merge(array, l, m, r);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int[] a = new int[N];
        for (int i = 0; i < a.length; ++i)
            a[i] = cin.nextInt();
        sort(a, 0, N - 1);
        for (int i = 0; i < N; ++i)
            System.out.print(a[i] + " ");
    }

}
