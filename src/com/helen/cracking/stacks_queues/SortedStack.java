package com.helen.cracking.stacks_queues;

import java.util.ListIterator;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by Helen on 01.01.2018.
 */
public class SortedStack {

    private Stack<Integer> stack = new Stack<Integer>();
    private Stack<Integer> auxStack = new Stack<Integer>();

    public void push(Integer item) {
        while (true) {
            if (stack.isEmpty() || stack.peek() < item)
                break;
            auxStack.push(stack.pop());
        }
        stack.push(item);
        while (!auxStack.isEmpty()) {
            stack.push(auxStack.pop());
        }
    }

    public Integer pop() {
        return stack.pop();
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }

    public void printQueue(boolean printSpace) {
        if (stack.isEmpty()) {
            System.out.println("e");
            return;
        }
        for (ListIterator iterator = stack.listIterator(); iterator.hasNext(); ) {
            System.out.print(iterator.next() + (printSpace ? " " : ""));
        }
        System.out.println();
    }

    public Integer peek() {
        return stack.peek();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        SortedStack queue = new SortedStack();
        for (int i = 0; i < N; ++i) {
            String s = scanner.nextLine();
            if (s.length() == 0) {
                i--;
                continue;
            }
            if (s.charAt(0) == '-') {
                System.out.println("Pop: " + queue.pop());
            } else if (s.charAt(0) == 'p') {
                queue.printQueue(true);
            } else {
                queue.push(Integer.parseInt(s));
            }
        }
    }


}
