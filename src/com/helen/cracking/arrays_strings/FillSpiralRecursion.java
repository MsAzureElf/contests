package com.helen.cracking.arrays_strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Helen on 07.01.2018.
 */
public class FillSpiralRecursion {

    private static boolean fillSpiral(int [][]a, int i, int j, int k, int dir){
        int N = a.length;
        if (i < 0 || j < 0 || i >= N || j >= N || a[i][j] != 0)
            return false;
        a[i][j] = k;
        switch (dir){
            case 0:
                if (fillSpiral(a, i, j+1, k+1, dir))
                    return true;
                else dir += 1;
            case 1:
                if (fillSpiral(a, i+1, j, k+1, dir))
                    return true;
                else dir += 1;
            case 2:
                if (fillSpiral(a, i, j-1, k+1, dir))
                    return true;
                else dir += 1;
            case 3:
                if (fillSpiral(a, i-1, j, k+1, dir))
                    return true;
                else dir = 0;
        }
        return fillSpiral(a, i, j+1, k+1, dir);
    }

    public static int[][] spiral(int N){
        int [][]a = new int[N][N];
        for (int i=0;i<N;++i)
            Arrays.fill(a[i], 0);
        fillSpiral(a, 0, 0, 1, 0);
        return a;
    }


    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int[][] a = spiral(N);
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j)
                System.out.print(String.format("%02d", a[i][j]) + " ");
            System.out.println();
        }
    }

}
