package com.helen.cracking.arrays_strings;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeSet;

public class SetZero {

    private class Cell{

        private int x, y;

        public Cell(int y, int x){
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }

    public int[][] setZero(int [][]a){
        if (a == null || a.length < 1)
            return a;
        ArrayList<Cell> zeroCells = new ArrayList<>();
        for (int i=0;i<a.length;++i)
            for (int j=0;j<a[0].length;++j)
                if (a[i][j] == 0)
                    zeroCells.add(new Cell(i, j));
        for (Cell cell : zeroCells){
            for (int i=0;i<a.length;++i)
                a[i][cell.getX()] = 0;
            for (int j=0;j<a[0].length;++j)
                a[cell.getY()][j] = 0;
        }
        return a;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(), M = scanner.nextInt(), cntRandoms = scanner.nextInt();
        int[][] image = new int[N][M];
        Random r = new Random();
        r.setSeed(System.currentTimeMillis());
        TreeSet<Integer> randoms = new TreeSet<>();
        for (int i=0;i<cntRandoms;++i)
            randoms.add(r.nextInt(N*M));
        for (int i = 0; i < N; ++i)
            for (int j = 0; j < M; ++j) {
                image[i][j] = i * M + j + 1;
                if (randoms.contains(image[i][j]))
                    image[i][j] = 0;
            }
        RotateImage rotate = new RotateImage();
        SetZero zero = new SetZero();
        rotate.printImage(image);
        System.out.println();
        image = zero.setZero(image);
        rotate.printImage(image);
    }

}
