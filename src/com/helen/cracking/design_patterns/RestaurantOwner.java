package com.helen.cracking.design_patterns;

// Singletone pattern
public class RestaurantOwner {

    private static RestaurantOwner restaurantOwner = null;

    private RestaurantOwner(){

    }

    public static RestaurantOwner getInstance(){
        if (restaurantOwner == null)
            restaurantOwner = new RestaurantOwner();
        return restaurantOwner;
    }

}