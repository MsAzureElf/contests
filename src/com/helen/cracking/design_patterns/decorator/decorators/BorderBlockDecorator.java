package com.helen.cracking.design_patterns.decorator.decorators;

import com.helen.cracking.design_patterns.decorator.AbstractBlock;
import com.helen.cracking.design_patterns.decorator.AbstractBlockDecorator;

public class BorderBlockDecorator extends AbstractBlockDecorator {

    private int borderWidth;

    public BorderBlockDecorator(AbstractBlock decoratee) {
        super(decoratee);
    }

    @Override
    public void draw() {
        decoratee.draw();
        drawBorder();
    }

    public void drawBorder(){
        System.out.println("Drawing border...");
    }

}
