package com.helen.cracking.design_patterns.deck_of_cards;

public class Card {

    public enum CardType {TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEn, JACK, QUEEN, KING, ACE};
    public enum CardSuit {CLUBS, SPADES, HEARTS, DIAMONDS};

    private CardType type;
    private CardSuit suit;

    public Card(CardType type, CardSuit suit){
        this.type = type;
        this.suit = suit;
    }

}
