package com.helen.cracking.design_patterns.decorator.blocks;

import com.helen.cracking.design_patterns.decorator.AbstractBlock;

public class TerminatorBlock implements AbstractBlock {

    @Override
    public void draw() {
        System.out.println("TerminatorBlock drawing...");
    }
}
