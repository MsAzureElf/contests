package com.helen.cracking.design_patterns.deck_of_cards;

import java.util.ArrayList;
import java.util.Collections;

public class DeckOfCards {

    ArrayList<Card> cards = new ArrayList<>();

    public DeckOfCards(){
        for (Card.CardSuit suit : Card.CardSuit.values()){
            for (Card.CardType type : Card.CardType.values()) {
                cards.add(new Card(type, suit));
            }
        }
        Collections.shuffle(cards);
    }

}
