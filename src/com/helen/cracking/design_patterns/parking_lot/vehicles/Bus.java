package com.helen.cracking.design_patterns.parking_lot.vehicles;

import com.helen.cracking.design_patterns.parking_lot.ParkingSpot;
import com.helen.cracking.design_patterns.parking_lot.Vehicle;

/**
 * Created by Helen on 03.01.2018.
 */
public class Bus extends Vehicle {

    public Bus(){
        this.size = VehicleSize.Large;
        this.spotsOccupied = 5;
    }

    @Override
    public boolean fitsInSpot(ParkingSpot spot) {
        return false;
    }
}
