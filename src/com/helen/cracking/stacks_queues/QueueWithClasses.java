package com.helen.cracking.stacks_queues;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

/**
 * Created by Helen on 01.01.2018.
 */
public class QueueWithClasses {

    private static int lastBornCatID = 0;
    private static int lastBornDogID = 0;

    private enum PetCategory {DOG, CAT};

    private static class Pet {

        private int id;
        private PetCategory petCategory;

        public Pet(PetCategory petCategory) {
            this.petCategory = petCategory;
            switch (petCategory) {
                case DOG:
                    this.id = lastBornDogID++;
                    break;
                case CAT:
                    this.id = lastBornCatID++;
            }
        }

        @Override
        public String toString(){
            return (petCategory==PetCategory.CAT?"c":"d")+id;
        }

    }

    private LinkedList<Pet> underlyingList = new LinkedList<>();

    public void enqueue(Pet pet){
        underlyingList.push(pet);
    }

    public Pet dequeue(){
        if (underlyingList.isEmpty())
            return null;
        return underlyingList.pollLast();
    }

    private Pet dequeueSpecific(PetCategory petCategory){
        if (underlyingList.isEmpty())
            return null;
        int count = 0, countSecondTime = 0;
        for (Iterator iterator = underlyingList.descendingIterator();iterator.hasNext();){
            if (((Pet)iterator.next()).petCategory == petCategory)
                break;
            count++;
        }
        if (count == underlyingList.size())
            return null;
        Pet pet = underlyingList.get(underlyingList.size()-count-1);
        underlyingList.remove(underlyingList.size()-count-1);
        return pet;
    }

    public Pet dequeueCat(){
        return dequeueSpecific(PetCategory.CAT);
    }

    public Pet dequeueDog(){
        return dequeueSpecific(PetCategory.DOG);
    }

    public void printQueue(boolean printSpace) {
        for (Iterator iterator = underlyingList.descendingIterator(); iterator.hasNext(); ) {
            System.out.print(iterator.next() + (printSpace ? " " : ""));
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        QueueWithClasses queue = new QueueWithClasses();
        for (int i = 0; i < N; ++i) {
            String s = scanner.nextLine();
            if (s.length() == 0) {
                i--;
                continue;
            }
            if (s.charAt(0) == '-') {
                if (s.length() > 1) {
                    char mode = s.charAt(1);
                    if (mode != 'c' && mode != 'd') {
                        System.out.println("Wrong format!");
                        continue;
                    }
                    System.out.println("Pop " + ((mode == 'c') ? "cat" : "dog") + ": " + ((mode == 'c') ? queue.dequeueCat() : queue.dequeueDog()));
                } else
                    System.out.println("Pop: " + queue.dequeue());
            } else if (s.charAt(0) == 'p') {
                queue.printQueue(true);
            }
            else if (s.charAt(0) == 'c'){
                queue.enqueue(new Pet(PetCategory.CAT));
            }
            else if (s.charAt(0) == 'd'){
                queue.enqueue(new Pet(PetCategory.DOG));
            }
            else {
                System.out.println("Unknown formatter!");
            }
        }
    }

}
