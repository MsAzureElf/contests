package com.helen.cracking.stacks_queues;

import java.util.Scanner;

public class Queue<T extends Comparable<T>> {

    public static class Node<T extends Comparable<T>> {

        T value;

        Node<T> next;

        public Node(T value) {
            this.value = value;
            this.next = null;
        }

        public T getValue() {
            return value;
        }
    }

    private Node<T> head, tail;

    public void enqueue(T item){
        Node<T> node = new Node<>(item);
        if (head == null){
            head = node;
            tail = node;
        } else {
            tail.next = node;
            tail = tail.next;
        }
    }

    public T dequeue(){
        if (head == null)
            return null;
        T item = head.getValue();
        head = head.next;
        return item;
    }

    private void printNode(Node node, boolean printSpace, boolean reverse){
        if (node == null)
            return;
        if (reverse) printNode(node.next, printSpace, reverse);
        System.out.print(node.getValue().toString() + (printSpace ? " " : ""));
        if (!reverse) printNode(node.next, printSpace, reverse);
    }

    public void printQueue(boolean printSpace, boolean reverse) {
        printNode(this.head, printSpace, reverse);
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        Queue<Integer> queue = new Queue<>();
        for (int i = 0; i < N; ++i) {
            String s = scanner.nextLine();
            if (s.length() == 0) {
                i--;
                continue;
            }
            if (s.charAt(0) == '-') {
                System.out.println("Dequeue: " + queue.dequeue());
            } else if (s.charAt(0) == 'p')
                queue.printQueue(true, false);
            else {
                queue.enqueue(Integer.parseInt(s));
            }
        }
    }

}
