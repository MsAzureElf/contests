package com.helen.cracking.trees_graphs;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Helen on 02.01.2018.
 */
public class BreadthFirstSearch {

    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();

    private static int[] used = null;

    public static void bfs(int i, int nComponent){
        if (graph == null || used[i] != 0)
            return;
        Queue<Integer> queue = new LinkedList<>();
        queue.add(i);
        while (!queue.isEmpty()){
            int v = queue.poll();
            if (used[v] != 0)
                continue;
            System.out.println("Vertex visited: " + (v+1));
            used[v] = nComponent;
            if (graph.size() <= v)
                continue;
            for (int j=0;j<graph.get(v).size();++j)
                if (used[graph.get(v).get(j)] == 0)
                    queue.add(graph.get(v).get(j));
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(), M = scanner.nextInt();
        for (int i=0;i<N;++i)
            graph.add(new ArrayList<Integer>());
        used = new int[N];
        Arrays.fill(used, 0);
        for (int i=0;i<M;++i){
            int v = scanner.nextInt() - 1, w = scanner.nextInt() - 1;
            graph.get(v).add(w);
            graph.get(w).add(v);
        }
        int componentCount = 0;
        for (int i=0;i<N;++i){
            if (used[i] == 0){
                componentCount++;
                bfs(i, componentCount);
            }
        }
        System.out.println("-------------------------------------");
        System.out.println("Component count: " + componentCount);
    }


}
