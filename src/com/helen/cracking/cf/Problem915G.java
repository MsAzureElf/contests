package com.helen.cracking.cf;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Helen on 03.02.2018.
 *
 * Simple arrays
 *
 */
public class Problem915G {

    public static int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int n = cin.nextInt(), k = cin.nextInt();
        int []primes = new int[n];
        for (int i=0;i<n;++i)
            primes[i] = i;
        int sqrt = (int)Math.sqrt(n) + 1;
        for (int i=2;i<sqrt;++i)
            for (int j=i;i*j<n;++j)
                primes[i*j] = i;
    }

}
