package com.helen.cracking.stacks_queues;

public class StackArrayBased {

    private static final int BASE_SIZE = 8;
    private int[] basis = null;
    private int[] sizes = null;
    private int stacksCount = 0;  // simultaniousStacksCount

    public StackArrayBased(int simultaniousStacksCount) {
        this.stacksCount = simultaniousStacksCount;
        basis = new int[BASE_SIZE * stacksCount];
        sizes = new int[stacksCount];
    }

    public Integer pop(int stackIndex){
        if (stackIndex < 0 || stackIndex >= stacksCount)
            return null;
        if (sizes[stackIndex] == 0)
            return null;
        int item = basis[stackIndex];
        shiftLeft(stackIndex);
        sizes[stackIndex]--;
        return item;
    }

    private void shiftLeft(int stackIndex){
        for (int i=0;i<sizes[stackIndex]-1;++i)
            basis[i*stacksCount+stackIndex] = basis[(i+1)*stacksCount+stackIndex];
    }

    private void shiftRight(int stackIndex){
        for (int i=sizes[stackIndex];i>0;--i)
            basis[i*stacksCount+stackIndex] = basis[(i-1)*stacksCount+stackIndex];
    }

    public Integer peek(int stackIndex){
        if (stackIndex < 0 || stackIndex >= stacksCount)
            return null;
        if (sizes[stackIndex] == 0)
            return null;
        return basis[stackIndex];
    }

    public void push(int stackIndex, int item){
        if (stackIndex < 0 || stackIndex >= stacksCount)
            return;
        if (basis.length < sizes[stackIndex]*stacksCount){
            int[] temp_basis = new int[sizes[stackIndex]*stacksCount];
            System.arraycopy(basis, 0, temp_basis, 0, basis.length);
            basis = temp_basis;
        }
        shiftRight(stackIndex);
        basis[stackIndex] = item;
        sizes[stackIndex]++;
    }

}
