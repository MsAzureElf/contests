package com.helen.cracking.cf;

import java.util.Scanner;

/**
 * Created by Helen on 05.02.2018.
 */
public class Problem36B {

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int n = cin.nextInt(), pos = cin.nextInt(), l = cin.nextInt(), r = cin.nextInt();
        if (l > 1 && r < n){
            int edgeDist = Math.min(Math.abs(pos-r), Math.abs(pos-l));
            if (pos >= l && pos <= r)
                System.out.println(edgeDist*2+2+Math.max(Math.abs(pos-r), Math.abs(pos-l)));
            else
                System.out.println((r-l)+2+edgeDist);
        } else if (l == 1 && r < n) {
            System.out.println(Math.abs(pos-r)+1);
        } else if (l > 1 && r == n){
            System.out.println(Math.abs(pos-l)+1);
        } else {
            System.out.println(0);
        }
    }

}
