package com.helen.cracking.design_patterns.facade;

import com.helen.cracking.design_patterns.parking_lot.ParkingLevel;
import com.helen.cracking.design_patterns.parking_lot.ParkingLot;
import com.helen.cracking.design_patterns.parking_lot.Vehicle;

public class Aggregator {

    private ParkingLot parkingLot;

    public Aggregator(){
        parkingLot = new ParkingLot();
    }

    public boolean parkVehicle(Vehicle v){
        return parkingLot.parkVehicle(v);
    }

    public int getFreeSpotsCount(int level){
        ParkingLevel parkingLevel = parkingLot.getParkingLevel(level);
        if (parkingLevel == null)
            return 0;
        return parkingLevel.getFreeSpotsCount();
    }

    public ParkingLevel getFirstFreeLevel(){
        int nLevels = parkingLot.getLevelsCount();
        for (int i=0;i<nLevels;++i)
            if (parkingLot.getParkingLevel(i).getFreeSpotsCount() > 0)
                return parkingLot.getParkingLevel(i);
        return null;
    }


}
