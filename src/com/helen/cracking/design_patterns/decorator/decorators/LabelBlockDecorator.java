package com.helen.cracking.design_patterns.decorator.decorators;

import com.helen.cracking.design_patterns.decorator.AbstractBlock;
import com.helen.cracking.design_patterns.decorator.AbstractBlockDecorator;

public class LabelBlockDecorator extends AbstractBlockDecorator {

    private String label;

    public LabelBlockDecorator(AbstractBlock decoratee) {
        super(decoratee);
    }

    @Override
    public void draw() {
        decoratee.draw();
        drawLabel();
    }

    public void drawLabel(){
        System.out.println("Drawing label...");
    }

}
