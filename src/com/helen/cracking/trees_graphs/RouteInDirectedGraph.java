package com.helen.cracking.trees_graphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Helen on 02.01.2018.
 */
public class RouteInDirectedGraph {

    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();

    private static int[] used = null;

    public static boolean dfs(int i, int searchedVertex){
        if (graph == null || graph.size() <= i)
            return false;
        if (searchedVertex == i)
            return true;
        used[i] = 1;
        boolean found = false;
        for (int j=0;j<graph.get(i).size();++j) {
            if (used[graph.get(i).get(j)] == 0) {
                found |= dfs(graph.get(i).get(j), searchedVertex);
            }
        }
        return found;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(), M = scanner.nextInt();
        for (int i=0;i<N;++i)
            graph.add(new ArrayList<Integer>());
        used = new int[N];
        Arrays.fill(used, 0);
        for (int i=0;i<M;++i){
            int v = scanner.nextInt() - 1, w = scanner.nextInt() - 1;
            graph.get(v).add(w);
        }
        int v1 = scanner.nextInt(), v2 = scanner.nextInt();
        System.out.println("Has route between " + v1 + " and " + v2 + ": " + dfs(v1-1, v2-1));
    }

}
