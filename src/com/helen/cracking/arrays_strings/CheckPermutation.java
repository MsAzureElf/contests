package com.helen.cracking.arrays_strings;

import java.util.Scanner;

public class CheckPermutation {

    private boolean isSubstring(String s, String base){
        return base.contains(s);
    }

    public boolean isPermutation(String s1, String s2){
        if (s1.length() != s2.length())
            return false;
        int N = s1.length();
        for (int i = 0;i < N; ++i){
            if (isSubstring(s1.substring(i), s2.substring(0, N-i)) && isSubstring(s1.substring(0, i), s2.substring(N-i))){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s1 = scanner.nextLine(), s2 = scanner.nextLine();
        System.out.println(new CheckPermutation().isPermutation(s1, s2));
    }

}
