package com.helen.cracking.testing;

import java.util.Arrays;

/**
 * Created by Helen on 03.01.2018.
 */
public class Utils {

    public int[] sort(int []array){
        int []copied_array = Arrays.copyOf(array, array.length);
        Arrays.sort(copied_array);
        return copied_array;
    }

}
