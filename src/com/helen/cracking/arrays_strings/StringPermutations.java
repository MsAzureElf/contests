package com.helen.cracking.arrays_strings;

import java.util.Scanner;

public class StringPermutations {

    public static void permut(int index, char[]a){
        if (index == a.length-1){
            System.out.println(new String(a));
            return;
        }
        char t;
        for (int i=index;i<a.length;++i){
            t = a[index];
            a[index] = a[i];
            a[i] = t;
            permut(index+1, a);
            a[i] = a[index];
            a[index] = t;
        }
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s1 = scanner.next();
        permut(0, s1.toCharArray());
    }

}
