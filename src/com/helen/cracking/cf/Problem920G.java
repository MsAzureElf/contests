package com.helen.cracking.cf;

import java.util.Scanner;

/**
 * Created by Helen on 03.02.2018.
 * <p>
 * List of numbers
 */

public class Problem920G {

    public static int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int T = cin.nextInt();
        for (int t = 0; t < T; ++t) {
            int x = cin.nextInt(), p = cin.nextInt(), k = cin.nextInt();
            int i = 0, m = x + 1;
            while (i < k) {
                if (gcd(m, p) == 1)
                    i++;
                if (i == k)
                    break;
                m++;
            }
            System.out.println(m);
        }
    }

}
