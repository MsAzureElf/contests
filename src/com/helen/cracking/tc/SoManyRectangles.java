package com.helen.cracking.tc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Created by Helen on 10.02.2018.
 */
public class SoManyRectangles {

    public static class Point implements Comparable<Point> {

        public long x1, y1;
        public long r, flag;
        public Point(long x1, long y1, int flag){
            this.x1 = x1;
            this.y1 = y1;
            r = (x1)*(x1)+(y1)*(y1);
            this.flag = flag;
        }

        @Override
        public int compareTo(Point o) {
            if (this.r < o.r)
                return -1;
            if (this.r > o.r)
                return 1;
            if (this.flag < o.flag)
                return -1;
            if (this.flag > o.flag)
                return 1;
            return 0;
        }

    }
    public static long maxOverlap(int[] x1, int[] y1, int[] x2, int[] y2){
        ArrayList<Point> a = new ArrayList<>();
        //PriorityQueue<Rect> pq = new PriorityQueue<>();
        for (int i=0;i<x1.length;++i) {
            a.add(new Point(x1[i], y1[i], 1));
            a.add(new Point(x2[i], y2[i], -1));
        }
        Collections.sort(a);
        int cnt = 0;
        int mx = 0;
        for (int i=0;i<a.size();++i) {
            cnt += a.get(i).flag;
            mx = Math.max(cnt, mx);
        }
        mx = Math.max(cnt, mx);
        /*while (pq.size() > 1){
            Rect rect1 = pq.poll(), rect2 = pq.poll();
            Rect overlap = overlap(rect1, rect2);
            if (overlap == null);
        }
        Rect res = pq.poll();
        return res.cnt;*/
        return mx;
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int []x1 = new int[N];
        int []y1 = new int[N];
        int []x2 = new int[N];
        int []y2 = new int[N];
        for (int i=0;i<N;++i) {
            x1[i] = cin.nextInt();
        }
        for (int i=0;i<N;++i) {
            y1[i] = cin.nextInt();
        }
        for (int i=0;i<N;++i) {
            x2[i] = cin.nextInt();
        }
        for (int i=0;i<N;++i) {
            y2[i] = cin.nextInt();
        }
        System.out.println(maxOverlap(x1, y1, x2, y2));
    }

}
