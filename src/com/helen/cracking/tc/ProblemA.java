package com.helen.cracking.tc;

import java.util.Scanner;

/**
 * Created by Helen on 10.02.2018.
 */
public class ProblemA {

    public static int minimumFixes(String[] board){
        int N = board.length, M = board[0].length();
        int [][]iBoard = new int[N][M];
        int [][]tBoard = new int[N][M];
        for (int i=0;i<N;++i)
            for (int j=0;j<M;++j) {
                tBoard[i][j] = (i%2==0&&j%2==0 || i%2==1&&j%2==1)?1:0;
                iBoard[i][j] = (board[i].charAt(j) == 'B') ? 1 : 0;
            }
        int cntOdd = 0, cntEven = 0;
        for (int i=0;i<N;++i)
            for (int j=0;j<M;++j) {
                cntOdd += Math.abs(iBoard[i][j] - tBoard[i][j]);
                cntEven += Math.abs(iBoard[i][j] - tBoard[i][j] + 1) % 2;
            }
        return Math.min(cntOdd, cntEven);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        String []a = new String[N];
        for (int i=0;i<N;++i)
            a[i] = cin.next();
        System.out.println(minimumFixes(a));
    }

}
