package com.helen.cracking.hashes;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by Helen on 27.12.2017.
 */
public class Reverse {

    public String reverseNullTerminated(StringBuffer toReverse) {
        int halfLength = toReverse.length() / 2;
        for (int i = 0; i < halfLength; ++i) {
            char t = toReverse.charAt(i);
            toReverse.setCharAt(i, toReverse.charAt(toReverse.length() - i - 1));
            toReverse.setCharAt(toReverse.length() - i - 1, t);
        }
        return toReverse.toString();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        Reverse reverse = new Reverse();
        String reversed = reverse.reverseNullTerminated(new StringBuffer(s));
        System.out.println(reversed);
        ArrayList<String> list1 = new ArrayList();
        list1.add(s);
        list1.add(reversed);
        ArrayList<String> list2 = new ArrayList();
        list2.add(s);
        list2.add(reversed.substring(0, reversed.length() - 1) + "*");
        System.out.println(reverse.isPermutationV2(list1));
        System.out.println(reverse.isPermutation(s, reversed.substring(0, reversed.length() - 1)));
        System.out.println(reverse.isPermutationV2(list2));
    }

    public boolean isPermutation(String s1, String s2) {
        if (s1.length() != s2.length())
            return false;
        char[] sorted1 = s1.toCharArray(), sorted2 = s2.toCharArray();
        Arrays.sort(sorted1);
        Arrays.sort(sorted2);
        for (int i = 0; i < sorted1.length; ++i)
            if (sorted1[i] != sorted2[i])
                return false;
        return true;
    }

    public boolean isPermutationV2(ArrayList<String> list) {
        if (list.size() == 0)
            return true;
        int len = list.get(0).length();
        for (String s : list) {
            if (s.length() != len)
                return false;
        }
        List<List<Character>> charList = new ArrayList();
        for (int i = 0; i < list.size(); ++i) {
            charList.add(new ArrayList<Character>());
            for (Character c : list.get(i).toCharArray()) {
                charList.get(i).add(c);
                Collections.sort(charList.get(i));
            }
        }
        for (int i = 1; i < charList.size(); ++i) {
            for (int j = 0; j < len; ++j)
                if (charList.get(i).get(j) != charList.get(0).get(j))
                    return false;
        }
        return true;
    }

}
