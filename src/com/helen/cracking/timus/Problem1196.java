package com.helen.cracking.timus;

import java.util.Scanner;

public class Problem1196 {

    private static boolean binSearch(int []a, int l, int r, int x){
        if (l > r)
            return false;
        if (l == r)
            return a[l] == x;
        int m = (l + r)/2;
        if (x == a[m])
            return true;
        if (x < a[m])
            return binSearch(a, l, m, x);
        return binSearch(a, m+1, r, x);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int []a = new int[N];
        for (int i=0;i<N;++i)
            a[i] = Integer.parseInt(cin.next());
        int M = cin.nextInt();
        int cnt = 0, x;
        for (int i=0;i<M;++i){
            x = Integer.parseInt(cin.next());
            if (binSearch(a, 0, N-1, x))
                cnt += 1;
        }
        System.out.println(cnt);
    }

}
