package com.helen.cracking.timus;

import java.util.Arrays;
import java.util.Scanner;

public class Problem1510 {

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int []a = new int[N];
        for (int i=0;i<N;++i)
            a[i] = Integer.parseInt(cin.next());
        Arrays.sort(a);
        int max = a[0], cntMax = 1, cnt = 1;
        for (int i=1;i<N;++i){
            if (a[i-1] != a[i]){
                if (cnt > cntMax){
                    max = a[i-1];
                    cntMax = cnt;
                }
                cnt = 1;
            } else {
                cnt += 1;
            }
        }
        if (cnt > cntMax){
            max = a[N-1];
        }
        System.out.println(max);
    }

}
