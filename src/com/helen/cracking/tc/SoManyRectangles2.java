package com.helen.cracking.tc;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Helen on 10.02.2018.
 */
public class SoManyRectangles2 {

    public static class Rect implements Comparable<Rect> {

        public long x1, x2, y1, y2, cnt = 0;
        public long r;
        public Rect(long x1, long y1, long x2, long y2){
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
            r = (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
            cnt = 1;
        }

        @Override
        public int compareTo(Rect o) {
            if (this.r < o.r)
                return -1;
            if (this.r > o.r)
                return 1;
            return 0;
        }

        public Rect(long x1, long y1, long x2, long y2, long cnt) {
            this(x1, y1, x2, y2);
            this.cnt = cnt;
        }

    }

    public static Rect overlap(Rect rect1, Rect rect2){
        if(rect1.x2 < rect2.x1 || rect2.y2 < rect1.y1 || rect1.x2 < rect2.x1 || rect1.y2 < rect2.y1)
            return null;
        long z1 = Math.max(rect2.x1, rect1.x1), z2 = Math.min(rect2.x2, rect1.x2);
        long c1 = Math.max(rect2.y1, rect1.y1), c2 = Math.min(rect2.y2, rect1.y2);
        if (z1 == z2 || c1 == c2){
            return null;
        }
        return new Rect(z1, c1, z2, c2, Math.max(rect1.cnt, rect2.cnt)+1);
    }

    public static long maxOverlap(int[] x1, int[] y1, int[] x2, int[] y2){
        ArrayList<Rect> a = new ArrayList<>();
        //PriorityQueue<Rect> pq = new PriorityQueue<>();
        for (int i=0;i<x1.length;++i)
            a.add(new Rect(x1[i], y1[i], x2[i], y2[i]));
        /*while (pq.size() > 1){
            Rect rect1 = pq.poll(), rect2 = pq.poll();
            Rect overlap = overlap(rect1, rect2);
            if (overlap == null);
        }
        Rect res = pq.poll();
        return res.cnt;*/
        return 0;
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        String []a = new String[N];
        for (int i=0;i<N;++i)
            a[i] = cin.next();
    }

}
