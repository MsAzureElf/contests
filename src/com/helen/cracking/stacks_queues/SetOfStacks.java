package com.helen.cracking.stacks_queues;

import java.util.ListIterator;
import java.util.Scanner;
import java.util.Vector;
import java.util.Stack;

public class SetOfStacks<T extends Comparable<T>> {

    private int maxBound = 0;

    private Vector<Stack<T>> stacks = new Vector<>();

    public SetOfStacks(int maxBound){
        this.maxBound = maxBound;
        stacks.add(new Stack<T>());
    }

    public T pop(){
        if (stacks.get(stacks.size()-1).size() == 0)
            return null;
        T item = stacks.get(stacks.size()-1).pop();
        reorganizeStacksRemove();
        return item;
    }

    private void reorganizeStacksAdd(){
        int N = stacks.size();
        for (int i=0;i<N-1;++i){
            if (stacks.get(i).size() > maxBound - 1){
                stacks.get(i+1).push(stacks.get(i).pop());
            }
        }
        if (stacks.get(N-1).size() > maxBound - 1){
            Stack<T> stack = new Stack<T>();
            stack.push(stacks.get(N-1).pop());
            stacks.add(stack);
        }
    }

    private void reorganizeStacksRemove(){
        int N = stacks.size();
        for (int i=N-1;i>0;--i)
            if (stacks.get(i).isEmpty())
                stacks.remove(i);
        for (int i=N-2;i>=0;--i){
            if (stacks.get(i).size() < maxBound - 1){
                stacks.get(i).push(stacks.get(i+1).pop());
                if (stacks.get(i+1).isEmpty())
                    stacks.remove(i+1);
            }
        }
    }

    public void push(T item){
        stacks.get(0).push(item);
        reorganizeStacksAdd();
    }

    public void printStack(boolean printSpace) {
        for (int i=0;i<stacks.size();++i){
            for (ListIterator iterator=stacks.get(i).listIterator();iterator.hasNext();){
                System.out.print(iterator.next()+(printSpace?" ":""));
            }
            System.out.println();
        }
        System.out.println();
    }

    public T popAt(int stackIndex){
        if (stackIndex < 0 || stackIndex >= stacks.size())
            return null;
        T item = stacks.get(stackIndex).pop();
        reorganizeStacksRemove();
        return item;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        SetOfStacks<Integer> stack = new SetOfStacks<>(4);
        for (int i = 0; i < N; ++i) {
            String s = scanner.nextLine();
            if (s.length() == 0) {
                i--;
                continue;
            }
            if (s.length() == 1 && s.charAt(0) == '-') {
                System.out.println("Pop: " + stack.pop());
            } else if (s.charAt(0) == '-'){
                int index = Integer.parseInt(s.substring(1));
                System.out.println("Pop at (" + index + "): " + stack.popAt(index));
            } else if (s.charAt(0) == 'p'){
                stack.printStack(true);
            }
            else {
                stack.push(Integer.parseInt(s));
            }
        }
    }


}
