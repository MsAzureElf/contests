package com.helen.cracking.linkedlists;


import java.util.Scanner;
import java.util.TreeSet;

public class SingleLinkedList<T extends Comparable<T>> {

    public static class Node<T extends Comparable<T>> {

        T value;

        Node<T> next;

        public Node(T value) {
            this.value = value;
            this.next = null;
        }

        public void addNext(Node<T> node) {
            this.next = node;
        }

        public boolean removeNext() {
            if (this.next == null)
                return false;
            else {
                this.next = this.next.next;
                return true;
            }
        }

        public boolean hasNext() {
            return this.next != null;
        }

        public Node<T> getNext() {
            return this.next;
        }

        public Node<T> appendToTail(Node<T> node) {
            Node<T> currentNode = this;
            while (currentNode.next != null)
                currentNode = currentNode.next;
            currentNode.next = node;
            return node;
        }

        public T getValue() {
            return value;
        }
    }

    private Node<T> head;

    public Node<T> getHead() {
        return head;
    }

    public SingleLinkedList() {
        head = null;
    }

    public Node<T> addNode(T value) {
        if (head == null) {
            head = new Node<T>(value);
            return head;
        } else {
            return head.appendToTail(new Node<T>(value));
        }
    }

    public SingleLinkedList<T> removeDuplicates() {
        TreeSet<T> set = new TreeSet<>();
        Node<T> currentNode = this.getHead();
        set.add(currentNode.getValue());
        while (currentNode.hasNext()) {
            if (set.contains(currentNode.getNext().getValue()))
                currentNode.removeNext();
            else {
                currentNode = currentNode.getNext();
                set.add(currentNode.getValue());
            }
        }
        return this;
    }

    public void printList(boolean printSpace) {
        Node<T> currentNode = this.getHead();
        while (currentNode != null) {
            System.out.print(currentNode.getValue().toString() + (printSpace?" ":""));
            currentNode = currentNode.getNext();
        }
        System.out.println();
    }

    public T getKthElement(int k) {
        int count = 1;
        Node<T> currentNode = this.getHead();
        while (currentNode != null && count < k) {
            currentNode = currentNode.getNext();
            count++;
        }
        return (currentNode == null) ? null : currentNode.getValue();
    }

    public void partition(T value) {
        if (this.getHead() == null || !this.getHead().hasNext())
            return;  // only one node - nothing to move
        SingleLinkedList<T> b = new SingleLinkedList<>();
        Node<T> tmpHead = new Node<T>(value);
        tmpHead.addNext(this.getHead());
        this.head = tmpHead;
        Node<T> currentNode = this.getHead();
        while (currentNode.getNext() != null)
            if (currentNode.getNext().getValue().compareTo(value) < 0) {
                Node<T> nextNextNode = currentNode.next.next;
                if (b.head == null) {
                    b.head = currentNode.getNext();
                    b.head.next = null;
                }
                else {
                    Node bEndNode = b.head;
                    while (bEndNode.next != null)
                        bEndNode = bEndNode.next;
                    bEndNode.next = currentNode.getNext();
                    bEndNode.next.next = null;
                }
                currentNode.next = nextNextNode;
            }
            else currentNode = currentNode.next;
        this.head = tmpHead.next;
        if (b.head == null)
            return;
        if (this.head == null)
            this.head = b.head;
        else {
            currentNode = b.head;
            while (currentNode.next != null)
                currentNode = currentNode.next;
            currentNode.next = this.head;
            this.head = b.head;
        }
    }

    private Node<T> add(Node<T> list1, Node<T> list2, Integer carry){
        if (list1 == null)
            return list2;
        if (list2 == null)
            return list1;
        Integer value1 = (Integer)list1.getValue(), value2 = (Integer)list2.getValue();
        Integer currentDigit = value1 + value2 + carry;
        Node<T> node = new Node(currentDigit % 10);
        node.next = add(list1.next, list2.next, currentDigit/10);
        return node;
    }

    private Node reverseList(Node currentNode, Node linkToHead){
        if (currentNode.next == null) {
            linkToHead.next = currentNode;
            return currentNode;
        }
        Node newParent = reverseList(currentNode.next, linkToHead);
        newParent.next = currentNode;
        return currentNode;
    }

    public void reverse() {
        Node<T> linkToHead = new Node<>(this.head.value);
        Node tail = reverseList(this.head, linkToHead);
        tail.next = null;
        this.head = linkToHead.next;
    }

    private Node getMiddle(){
        Node iterator1 = this.head;
        if (this.head == null)
            return null;
        if (this.head.next == null)
            return head;
        Node iterator2 = this.head;
        while (iterator1 != null && iterator2.next != null && iterator2.next.next != null){
            iterator1 = iterator1.next;
            iterator2 = iterator2.next.next;
        }
        return iterator1;
    }

    private int countNodes(){
        Node iterator = this.head;
        int count = 0;
        while (iterator != null){
            count += 1;
            iterator = iterator.next;
        }
        return count;
    }

    private boolean checkNodes(Node<T> it1, Node<T> it2) {
        if (it2.next.next == null || it1.next.next == null) {
            return it1.next.value.compareTo(it2.next.value) == 0;
        }
        it2.next = it2.next.next;
        boolean resultPrev = checkNodes(it1, it2);
        boolean resultCur = it1.next.value.compareTo(it2.next.value) == 0;
        it1.next = it1.next.next;
        return resultPrev && resultCur;
    }

    private SingleLinkedList<T> copyList(){
        Node<T> head = copyNode(this.head);
        SingleLinkedList<T> list = new SingleLinkedList<T>();
        list.head = head;
        return list;
    }

    private Node<T> copyNode(Node<T> node){
        if (node == null)
            return null;
        Node<T> copy = new Node<T>(node.getValue());
        copy.next = copyNode(node.next);
        return copy;
    }

    private boolean compare(Node<T> iterator1, Node<T> iterator2) {
        if (iterator1 == null && iterator2 == null)
            return true;
        if (iterator1 == null && iterator2 != null || iterator1 != null && iterator2 == null)
            return false;
        boolean curResult = (iterator1.value.compareTo(iterator2.value) == 0);
        return curResult && compare(iterator1.next, iterator2.next);
    }

    /*public boolean isPalindrome(){
        if (this.head == null || this.head.next == null)
            return true;
        int count = this.countNodes();
        Node<T> iterator1 = new Node(head.getValue());
        iterator1.next = head;
        Node<T> iterator2 = new Node(head.getValue());
        iterator2.next = getMiddle().next;
        return checkNodes(iterator1, iterator2);
    }*/

    public boolean isPalindrome(){
        SingleLinkedList<T> copy = copyList();
        copy.reverse();
        return compare(this.head, copy.head);
    }


    public SingleLinkedList<T> add(SingleLinkedList<T> list){
        /*if (list.head != null)
            list.reverse();
        if (this.head != null)
            this.reverse();*/
        Node<T> node = add(this.getHead(), list.getHead(), 0);
        this.head = node;
        this.reverse();
        return this;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        /*int N = scanner.nextInt();
        SingleLinkedList<Integer> list = new SingleLinkedList<>();
        for (int i = 0; i < N; ++i) {
            list.addNode(scanner.nextInt());
        }
        //list.removeDuplicates();
        list.printList();
        System.out.println(list.getKthElement(1));
        System.out.println(list.getKthElement(10));
        System.out.println(list.getKthElement(2));
        System.out.println(list.getKthElement(11));
        list.partition(3);
        list.printList();*/
        String number1 = scanner.next(), number2 = scanner.next();
        SingleLinkedList<Integer> list1 = new SingleLinkedList<>();
        for (int i = 0; i < number1.length(); ++i) {
            list1.addNode(number1.charAt(number1.length() - i - 1) - '0');
        }
        list1.printList(false);
        System.out.println("isPalindrome: " + list1.isPalindrome());
        System.out.println("Count nodes: " + list1.countNodes());
        System.out.println("Middle value: " + list1.getMiddle().getValue().toString());
        SingleLinkedList<Integer> list2 = new SingleLinkedList<>();
        for (int i = 0; i < number2.length(); ++i) {
            list2.addNode(number2.charAt(number2.length() - i - 1) - '0');
        }
        list1.add(list2);
        list1.printList(false);
    }


}
