package com.helen.cracking.geometry;

import java.util.Scanner;

/**
 * Created by Helen on 27.01.2018.
 */
public class OverlappingRectangles {

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int a1 = cin.nextInt(), b1 = cin.nextInt();
        int a2 = cin.nextInt(), b2 = cin.nextInt();
        int x1 = cin.nextInt(), y1 = cin.nextInt();
        int x2 = cin.nextInt(), y2 = cin.nextInt();
        if (a2 < x1 || b2 < y1 || x2 < a1 || y2 < b1){
            System.out.println("No rectangle");
            return;
        }
        int z1 = Math.max(a1, x1), z2 = Math.min(a2, x2);
        int c1 = Math.max(b1, y1), c2 = Math.min(b2, y2);
        if (z1 == z2 || c1 == c2){
            System.out.println("No rectangle");
            return;
        }
        System.out.println(String.format("(%d, %d), (%d, %d)", z1, c1, z2, c2));
    }

}
