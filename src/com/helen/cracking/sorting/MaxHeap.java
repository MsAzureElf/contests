package com.helen.cracking.sorting;

import java.util.Arrays;

/**
 * Created by Helen on 27.01.2018.
 */
public class MaxHeap {

    private int[] heap;
    private int size = 0;

    public MaxHeap(int[] array) {
        size = array.length;
        heap = Arrays.copyOf(array, size);
        for (int i = size / 2 - 1; i >= 0; --i)
            heapify(i);
    }

    public int getMax(){
        return heap[0];
    }

    public int replaceMax(int value){
        int max = heap[0];
        heap[0] = value;
        heapify(0);
        return max;
    }

    public void add(int value) {
        heap = Arrays.copyOf(heap, size+1);
        heap[size] = value;
        up(size);
        size++;
    }

    private void swap(int i, int j) {
        int t = heap[i];
        heap[i] = heap[j];
        heap[j] = t;
    }

    private void heapify(int i) {
        int largest = i;
        int li = 2 * i + 1, ri = 2 * i + 2;
        if (li < size && heap[largest] < heap[li]) {
            largest = li;
        }
        if (ri < size && heap[largest] < heap[ri]) {
            largest = ri;
        }
        if (largest != i) {
            swap(i, largest);
            heapify(largest);
        }
    }

    private void up(int i){
        if (i == 0)
            return;
        int parent = (i-1)/2;
        if (heap[i] > heap[parent]){
            swap(i, parent);
            up(parent);
        }
    }

    public void printHeap() {
        for (int i = 0; i < size; ++i) {
            System.out.print(heap[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int[] a = new int[]{19, 15, 3, -2, 6, 8, 10, 11, 4, 56, 31, -4};
        MaxHeap heap = new MaxHeap(a);
        heap.printHeap();
        heap.add(27);
        heap.printHeap();
    }

}
