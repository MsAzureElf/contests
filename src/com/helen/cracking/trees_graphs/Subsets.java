package com.helen.cracking.trees_graphs;

import java.util.Scanner;

/**
 * Created by Helen on 28.01.2018.
 */
public class Subsets {

    public static void printAllSubsets(int []a, int currentPos, int []b){
        if (currentPos == a.length){
            for (int i=0;i<b.length;++i)
                if (b[i] != 0)
                    System.out.print(a[i]+", ");
            System.out.println();
            return;
        }
        b[currentPos] = 0;
        printAllSubsets(a, currentPos+1, b);
        b[currentPos] = 1;
        printAllSubsets(a, currentPos+1, b);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int []a = new int[N];
        for (int i=0;i<N;++i){
            a[i] = cin.nextInt();
        }
        printAllSubsets(a, 0, new int[N]);
    }

}
