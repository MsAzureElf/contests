package com.helen.cracking.design_patterns.proxy.cells;

import com.helen.cracking.design_patterns.proxy.Cell;

public class Empty extends Cell {

    public Empty(int left, int top) {
        super(left, top);
    }

    @Override
    public int getPoints() {
        return 10;
    }
}
