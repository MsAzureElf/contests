package com.helen.cracking.trees_graphs;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Arrays;

/**
 * Created by Helen on 02.01.2018.
 */
public class DepthFirstSearch {

    private static ArrayList<ArrayList<Integer>> graph = new ArrayList<>();

    private enum Order {PRE_ORDER, IN_ORDER, POST_ORDER};

    private static int[] used = null;

    public static Integer dfs(Order order, int i, int p, int nComponent){
        if (graph == null || graph.size() <= i)
            return null;
        used[i] = nComponent;
        if (order == Order.PRE_ORDER)
            System.out.println("Visiting vertex: " + (i+1));
        if (order == Order.IN_ORDER && graph.get(i).size()==1)
            System.out.println("Visiting vertex: " + (i+1));
        Integer hasCycle = null;
        for (int j=0;j<graph.get(i).size();++j) {
            if (used[graph.get(i).get(j)] == nComponent && graph.get(i).get(j) != p)
                hasCycle = i;
            if (used[graph.get(i).get(j)] == 0) {
                Integer tempHasCycle = dfs(order, graph.get(i).get(j), i, nComponent);
                if (tempHasCycle != null)
                    hasCycle = tempHasCycle;
            }
            if (order == Order.IN_ORDER && ((j==0 && graph.get(i).get(j) != p) || (j == 1 && graph.get(i).get(j-1) == p)))
                System.out.println("Visiting vertex: " + (i+1));
        }
        if (order == Order.POST_ORDER)
            System.out.println("Visited vertex: " + (i+1));
        return hasCycle;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt(), M = scanner.nextInt();
        for (int i=0;i<N;++i)
            graph.add(new ArrayList<Integer>());
        used = new int[N];
        Arrays.fill(used, 0);
        for (int i=0;i<M;++i){
            int v = scanner.nextInt() - 1, w = scanner.nextInt() - 1;
            graph.get(v).add(w);
            graph.get(w).add(v);
        }
        int componentCount = 0;
        for (int i=0;i<N;++i)
            if (used[i] == 0){
                Integer cycleVertex = dfs(Order.PRE_ORDER, i, -1, componentCount+1);
                if (cycleVertex != null)
                    System.out.println("Cycle involving vertex " + (cycleVertex+1));
                else
                    System.out.println("No cycle in this connected component");
                componentCount++;
            }
        System.out.println(componentCount);
        System.out.println("-------------------------------------");
        Arrays.fill(used, 0);
        dfs(Order.IN_ORDER, 0, -1, 1);
        System.out.println("-------------------------------------");
        Arrays.fill(used, 0);
        dfs(Order.POST_ORDER, 0, -1, 1);
    }

}
