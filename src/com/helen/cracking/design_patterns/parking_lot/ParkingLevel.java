package com.helen.cracking.design_patterns.parking_lot;

/**
 * Created by Helen on 03.01.2018.
 */
public class ParkingLevel {

    public static int LEVEL_CAPACITY = 20;

    private ParkingSpot[] spots = new ParkingSpot[LEVEL_CAPACITY];
    private int freeSpots = LEVEL_CAPACITY;

    public ParkingLevel(){
        for (int i=0;i<LEVEL_CAPACITY;++i)
            spots[i] = new ParkingSpot(Vehicle.VehicleSize.Small);
    }

    public boolean parkVehicle(Vehicle vehicle){
        return true;
    }

    private ParkingSpot[] listAvailableSpots(){
        return spots;
    }

    public int getFreeSpotsCount(){
        return freeSpots;
    }

}
