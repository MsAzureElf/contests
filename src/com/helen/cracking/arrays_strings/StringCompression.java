package com.helen.cracking.arrays_strings;

import java.util.ArrayList;
import java.util.Scanner;

public class StringCompression {

    private class CompressedChars {

        private char character;
        private int count;

        public CompressedChars(char c) {
            character = c;
            count = 1;
        }

        void increaseCount() {
            count += 1;
        }

        public char getCharacter() {
            return character;
        }

        @Override
        public String toString(){
            return character + Integer.toString(count);
        }

    }

    public String compressString(String s) {
        char[] array = s.toCharArray();
        ArrayList<CompressedChars> list = new ArrayList<>();
        for (char c : array){
            if (list.isEmpty() || list.get(list.size()-1).getCharacter() != c)
                list.add(new CompressedChars(c));
            else
                list.get(list.size()-1).increaseCount();
        }
        StringBuffer result = new StringBuffer();
        for (CompressedChars cc : list)
            result.append(cc.toString());
        String resultString = result.toString();
        if (resultString.length() >= s.length())
            return s;
        else return resultString;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        System.out.println(new StringCompression().compressString(line));
    }
}
