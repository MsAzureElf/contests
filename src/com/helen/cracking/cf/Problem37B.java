package com.helen.cracking.cf;

import java.util.Scanner;

/**
 * Created by Helen on 07.02.2018.
 */
public class Problem37B {

    private static class Student implements Comparable<Student> {
        int l, r, id;

        public Student(int l, int r, int id){
            this.l = l;
            this.r = r;
            this.id = id;
        }

        @Override
        public int compareTo(Student o) {
            if (this.l < o.l)
                return -1;
            if (this.l > o.l)
                return 1;
            if (this.id < o.id)
                return -1;
            if (this.id > o.id)
                return 1;
            return 0;
        }
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int T = cin.nextInt();
        for (int t=0;t<T;++t) {
            int N = cin.nextInt();
            Student[] array = new Student[N];
            for (int i=0;i<N;++i){
                int l = cin.nextInt(), r = cin.nextInt();
                array[i] = new Student(l, r, i);
            }
        }
        System.out.println();
    }

}
