package com.helen.cracking.hashes;

import java.util.Scanner;

/**
 * Created by Helen on 27.12.2017.
 */
public class ReplaceSpaces {

    public char[] replaceSpaces(char[] array) {
        int cntSpaces = 0;
        for (char anArray : array) {
            if (anArray == ' ')
                cntSpaces++;
        }
        if (cntSpaces == 0)
            return array;
        int cntExtraChars = cntSpaces * 2;
        int arrayEnd = array.length - 1;
        while (arrayEnd >= 0 && array[arrayEnd] == 0)
            arrayEnd--;
        if (arrayEnd < 0)
            return array;
        for (int i = arrayEnd; i >= 0; --i) {
            if (array[i] == ' ') {
                cntExtraChars -= 2;
                array[i + cntExtraChars] = '%';
                array[i + cntExtraChars + 1] = '2';
                array[i + cntExtraChars + 2] = '0';
            } else
                array[i + cntExtraChars] = array[i];
        }
        return array;
    }

    private char[] fillArray(char[] initialArray, String s) {
        char[] sArray = s.toCharArray();
        if (initialArray.length < sArray.length)
            return sArray;
        for (int i = 0; i < sArray.length; ++i)
            initialArray[i] = sArray[i];
        return initialArray;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        ReplaceSpaces cur = new ReplaceSpaces();
        char[] array = cur.fillArray(new char[s.length() * 3], s);
        System.out.println(cur.replaceSpaces(array));
    }

}
