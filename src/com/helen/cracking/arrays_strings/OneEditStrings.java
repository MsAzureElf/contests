package com.helen.cracking.arrays_strings;

/**
 * Created by Helen on 07.01.2018.
 */

import java.util.Arrays;
import java.util.Scanner;

public class OneEditStrings {

    private static boolean oneEdit(String s1, String s2){
        char[] a1 =s1.toCharArray(), a2 = s2.toCharArray();
        if (Math.abs(a1.length-a2.length) > 1)
            return false;
        if (a1.length == 0 || a2.length == 0)
            return true;
        int diff = 0, counter = 0;
        boolean equalLengths = (a1.length == a2.length);
        while (counter < a2.length && counter+diff < a1.length){
            if (equalLengths){
                if (a1[counter] != a2[counter])
                    diff++;
            } else {
                if (a1[counter+diff] != a2[counter]){
                    diff += (a1.length > a2.length)?1:-1;
                    if (diff > 0)
                        counter--;
                }
            }
            if (Math.abs(diff) > 1)
                return false;
            counter++;
        }
        return true;
    }

    public static void main(String [] args){
        Scanner cin = new Scanner(System.in);
        String s1 = cin.next(), s2 = cin.next();
        System.out.println(oneEdit(s1, s2)?"Yes":"No");
    }

}
