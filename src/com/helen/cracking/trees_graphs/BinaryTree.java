package com.helen.cracking.trees_graphs;

import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;

/**
 * Created by Helen on 02.01.2018.
 */
public class BinaryTree {

    private static class TreeNode{

        private int number = 0;
        private TreeNode leftChild, rightChild, parent;

        public TreeNode(int number, TreeNode parent){
            this.number = number;
            this.parent = parent;
            this.leftChild = null;
            this.rightChild = null;
        }

        public TreeNode getLeftChild() {
            return leftChild;
        }

        public void setLeftChild(TreeNode leftChild) {
            this.leftChild = leftChild;
        }

        public TreeNode getRightChild() {
            return rightChild;
        }

        public void setRightChild(TreeNode rightChild) {
            this.rightChild = rightChild;
        }

        public TreeNode getParent() {
            return parent;
        }
    }

    private static TreeNode root = new TreeNode(0, null);

    private static class IntPair {

        private int height = 0;
        private int maxHeightDiff = 0;

        public IntPair(int height, int maxHeightDiff){
            this.height = height;
            this.maxHeightDiff = maxHeightDiff;
        }

        public int getHeight() {
            return height;
        }

        public int getMaxHeightDiff() {
            return maxHeightDiff;
        }
    }

    private static int getMaxHeight(TreeNode node){
        if (node == null || (node.getLeftChild() == null && node.getRightChild() == null))
            return 0;
        int heightLeft = getMaxHeight(node.getLeftChild());
        int heightRight = getMaxHeight(node.getRightChild());
        return Math.max(heightLeft, heightRight) + 1;
    }

    private static int getMinHeight(TreeNode node){
        if (node == null || (node.getLeftChild() == null && node.getRightChild() == null))
            return 0;
        int heightLeft = getMinHeight(node.getLeftChild());
        int heightRight = getMinHeight(node.getRightChild());
        return Math.min(heightLeft, heightRight) + 1;
    }

    public static boolean isBalanced(TreeNode tree){
        int maxHeight = getMaxHeight(tree), minHeight = getMinHeight(tree);
        if (maxHeight - minHeight > 1)
            return false;
        return true;
    }

    private static boolean addChild(TreeNode curRoot, int from, int to){
        if (curRoot.number == from){
            if (curRoot.getLeftChild() == null)
                curRoot.setLeftChild(new TreeNode(to, curRoot));
            else if (curRoot.getRightChild() == null)
                curRoot.setRightChild(new TreeNode(to, curRoot));
            else return false;
            return true;
        }
        if (curRoot.getLeftChild() != null)
            if (addChild(curRoot.getLeftChild(), from, to))
                return true;
        if (curRoot.getRightChild() != null)
            return addChild(curRoot.getRightChild(), from, to);
        return false;
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        for (int i=0;i<N-1;++i){
            int v = cin.nextInt() - 1, w = cin.nextInt() - 1;
            if (!addChild(root, v, w))
                System.out.println("Unable to add vertex " + (w+1));
        }
        System.out.println("Is balanced: " + isBalanced(root));
    }

}
