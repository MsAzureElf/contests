package com.helen.cracking.java8;

public class Lemur implements Mammal {

    private int age = 0;

    @Override
    public int getAge() {
        return age;
    }

}
