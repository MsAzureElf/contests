package com.helen.cracking.geometry;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Helen on 27.01.2018.
 */
public class ClosestPoints {

    public static class Pair implements Comparable<Pair> {

        public long x, y;
        private long r;

        public Pair(int x, int y){
            this.x = x;
            this.y = y;
            r = x*x + y*y;
        }

        public int compareTo(Pair p){
            if (this.r < p.r)
                return -1;
            if (this.r > p.r)
                return 1;
            return 0;
        }

    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt(), K = cin.nextInt();
        Pair[] array = new Pair[N];
        int x, y;
        for (int i=0;i<N;++i){
            x = cin.nextInt();
            y = cin.nextInt();
            array[i] = new Pair(x, y);
        }
        Arrays.sort(array);
        K = Math.min(N, K);
        for (int i=0;i<K;++i){
            System.out.println(array[i].x + " " + array[i].y);
        }
    }

}
