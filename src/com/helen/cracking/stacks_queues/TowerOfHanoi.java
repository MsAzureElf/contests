package com.helen.cracking.stacks_queues;

import java.util.ListIterator;
import java.util.Stack;
import java.util.Scanner;

/**
 * Created by Helen on 01.01.2018.
 */
public class TowerOfHanoi {

    public static void printStack(Stack stack, boolean printSpace) {
        if (stack.isEmpty()) {
            System.out.println("e");
            return;
        }
        for (ListIterator iterator = stack.listIterator(); iterator.hasNext(); ) {
            System.out.print(iterator.next() + (printSpace ? " " : ""));
        }
        System.out.println();
    }

    public static void hanoi(Stack<Integer>[] towers, int depth, int srcTower, int destTower){
        if (depth == 1){
            towers[destTower].push(towers[srcTower].pop());
            for (Stack<Integer> stack : towers){
                printStack(stack, true);
            }
            System.out.println("--------------------------------------------------------");
            return;
        }
        hanoi(towers, depth-1, srcTower, 3-srcTower-destTower);
        hanoi(towers, 1, srcTower, destTower);
        hanoi(towers, depth-1, 3-srcTower-destTower, destTower);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();  // number of items in first hanoi tower
        Stack<Integer>[] towers = new Stack[3];
        for (int i = 0; i < towers.length; ++i)
            towers[i] = new Stack<>();
        for (int i=N;i>0;--i)
            towers[0].push(i);
        for (Stack<Integer> stack : towers){
            printStack(stack, true);
        }
        System.out.println("--------------------------------------------------------");
        hanoi(towers, N, 0, 2);
    }

}
