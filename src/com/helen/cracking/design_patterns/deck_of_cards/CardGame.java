package com.helen.cracking.design_patterns.deck_of_cards;

import com.helen.cracking.design_patterns.deck_of_cards.game.BlackJack;
import com.helen.cracking.design_patterns.deck_of_cards.game.ClassicalPoker;
import com.helen.cracking.design_patterns.deck_of_cards.game.TexasHoldem;

//Factory pattern
public class CardGame {

    public enum CardGameType {CLASSICAL_POKER, DURAK, TEXAS_HOLDEM, BLACKJACK};

    public static CardGame createCardGame(CardGameType gameType) throws UnsupportedOperationException {
        switch (gameType){
            case DURAK:
                throw new UnsupportedOperationException("This CardGame type is not supported yet");
            case BLACKJACK:
                return new BlackJack();
            case CLASSICAL_POKER:
                return new ClassicalPoker();
            case TEXAS_HOLDEM:
                return new TexasHoldem();
        }
        return null;
    }

}
