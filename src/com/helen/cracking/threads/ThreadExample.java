package com.helen.cracking.threads;

public class ThreadExample extends Thread {

    private static final int COUNT_MAX = 5;
    private int count = 0;

    public void run(){
        System.out.println("Thread started.");
        try {
            while (count < COUNT_MAX){
                count++;
                System.out.println("Count: " + count);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread interrupted.");
        }
        System.out.println("Thread finished.");
    }

    public int getCount(){
        return count;
    }

    public static void main(String[] args) {
        ThreadExample thread = new ThreadExample();
        thread.start();
        while (thread.getCount() < 5){
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
