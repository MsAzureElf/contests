package com.helen.cracking.cf;

import java.util.Scanner;

/**
 * Created by Helen on 05.02.2018.
 */
public class Problem36A {

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int n = cin.nextInt(), k = cin.nextInt(), a;
        int maxHoursCount = Integer.MAX_VALUE;
        for (int i=0;i<n;++i){
            a = cin.nextInt();
            if (k % a == 0){
                maxHoursCount = Math.min(maxHoursCount, k/a);
            }
        }
        System.out.println(maxHoursCount);
    }

}
