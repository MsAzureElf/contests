package com.helen.cracking.arrays_strings;

import java.util.Scanner;

/**
 * Created by Helen on 07.01.2018.
 */
public class FillSpiral {

    public static int[][] spiral(int N) {
        int[][] a = new int[N][N];
        int k = 1;
        for (int i = 0; i < N / 2; ++i) {
            for (int j = i; j < N - i - 1; ++j) {
                int m = k + j - i;
                a[i][j] = m;
                a[j][N - i - 1] = m + (N - i * 2 - 1);
                a[N - i - 1][N - j - 1] = m + (N - i * 2 - 1) * 2;
                a[N - j - 1][i] = m + (N - i * 2 - 1) * 3;
            }
            k += 4 * (N - i * 2 - 1);
        }
        if (N % 2 > 0)
            a[N / 2][N / 2] = N * N;
        return a;
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int[][] a = spiral(N);
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j)
                System.out.print(String.format("%02d", a[i][j]) + " ");
            System.out.println();
        }
    }

}
