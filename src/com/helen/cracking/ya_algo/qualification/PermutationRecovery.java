package com.helen.cracking.ya_algo.qualification;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class PermutationRecovery {

    private static boolean checkIsRow(int [][]rows, int cntRows, int[]t, int N){
        for (int i=0;i<cntRows;++i){
            for (int j=0;j<N;++j)
                for (int k=0;k<N;++k)
                    if (rows[i][j] == t[k])
                        return false;
        }
        return true;
    }

    private static void swapRows(int [][]rows, int row1, int row2, int N){
        int []t = new int[N];
        System.arraycopy(rows[row1], 0,t, 0, N);
        System.arraycopy(rows[row2], 0,rows[row1], 0, N);
        System.arraycopy(t, 0,rows[row2], 0, N);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int []t = new int[N];
        int [][]rows = new int[N][N];
        int [][]columns = new int[N][N];
        int cntRows = 0, cntCols = 0;
        for (int i=0;i<2*N;++i){
            for (int j=0;j<N;++j)
                t[j] = cin.nextInt();
            boolean isRow = checkIsRow(rows, cntRows, t, N);
            if (isRow) {
                System.arraycopy(t, 0, rows[cntRows], 0, N);
                cntRows++;
            } else {
                System.arraycopy(t, 0, columns[cntCols], 0, N);
                cntCols++;
            }
        }
        int colIndex = 0;
        for (int i=0;i<N;++i){
            for (int j=0;j<N;++j) {
                if (columns[i][j] == rows[0][0]) {
                    colIndex = i;
                    break;
                }
            }
        }
        for (int i=0;i<N;++i){
            int row = 0;
            for (int j=0;j<N;++j){
                if (rows[j][0] == columns[colIndex][i]){
                    row = j;
                    break;
                }
            }
            swapRows(rows, row, i, N);
        }
        for (int i=0;i<N;++i){
            for (int j=0;j<N;++j){
                System.out.print(rows[i][j]+" ");
            }
        }
        System.out.println();
    }

}
