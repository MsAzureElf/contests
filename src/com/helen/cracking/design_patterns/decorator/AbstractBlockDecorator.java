package com.helen.cracking.design_patterns.decorator;

public abstract class AbstractBlockDecorator implements AbstractBlock {

    protected AbstractBlock decoratee;

    public AbstractBlockDecorator(AbstractBlock decoratee){
        this.decoratee = decoratee;
    }

    @Override
    public void draw(){
        decoratee.draw();
    }

}
