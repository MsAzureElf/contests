package com.helen.cracking.arrays_strings;

import java.util.Scanner;

public class RotateImage {

    private int[][] swap(int[][] image, int i, int j) {
        int temp = image[i][j];
        int N = image.length;
        image[i][j] = image[N - 1 - j][i];
        image[N - 1 - j][i] = image[N - 1 - i][N - 1 - j];
        image[N - 1 - i][N - 1 - j] = image[j][N - 1 - i];
        image[j][N - 1 - i] = temp;
        return image;
    }

    public int[][] doRotate(int[][] image) {
        int N = image.length;
        for (int i = 0; i < N / 2; ++i)
            for (int j = i; j < N - 1 - i; ++j)
                image = swap(image, i, j);
        return image;
    }

    public void printImage(int[][] image) {
        if (image == null || image.length < 1)
            return;
        for (int[] anImage : image) {
            for (int j = 0; j < image[0].length; ++j)
                System.out.print(String.format("%02d ", anImage[j]));
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int[][] image = new int[N][N];
        for (int i = 0; i < N; ++i)
            for (int j = 0; j < N; ++j)
                image[i][j] = i * N + j + 1;
        RotateImage rotate = new RotateImage();
        rotate.printImage(image);
        System.out.println();
        image = rotate.doRotate(image);
        rotate.printImage(image);
        System.out.println();
        image = rotate.doRotate(image);
        rotate.printImage(image);
    }

}
