package com.helen.cracking.trees_graphs;

import sun.reflect.generics.tree.Tree;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Helen on 02.01.2018.
 */
public class BinaryTreeFromSortedArray {


    private static class TreeNode{

        private int item = 0;
        private TreeNode leftChild, rightChild, parent;

        public TreeNode(int item, TreeNode parent){
            this.item = item;
            this.parent = parent;
            this.leftChild = null;
            this.rightChild = null;
        }

        public TreeNode getLeftChild() {
            return leftChild;
        }

        public void setLeftChild(TreeNode leftChild) {
            this.leftChild = leftChild;
        }

        public TreeNode getRightChild() {
            return rightChild;
        }

        public void setRightChild(TreeNode rightChild) {
            this.rightChild = rightChild;
        }

        public TreeNode getParent() {
            return parent;
        }

        public int getItem() {
            return item;
        }
    }

    private static TreeNode root = null;

    private static void printTree(){
        ArrayList<ArrayList<Integer>> printedList = new ArrayList<>();
        printTree(root, printedList, 0);
        int N = printedList.size();
        for (int i=0;i<printedList.size();++i){
            for (int j=0;j<printedList.get(i).size();++j) {
                for (int k=1;k<Math.pow(2, N-i-1);++k)
                    System.out.print("   ");
                System.out.print(String.format("%02d ", printedList.get(i).get(j)));
            }
            System.out.println();
        }
    }

    private static void printTree(TreeNode node, ArrayList<ArrayList<Integer>> printedList, int curHeight){
        if (printedList.size() < curHeight+1)
            printedList.add(new ArrayList<Integer>());
        printedList.get(curHeight).add(node.getItem());
        if (node.getLeftChild() != null)
            printTree(node.getLeftChild(), printedList, curHeight+1);
        if (node.getRightChild() != null)
            printTree(node.getRightChild(), printedList, curHeight+1);
    }

    private static TreeNode addNode(TreeNode node, int value){
        if (node == null){
            return new TreeNode(value, null);
        }
        if (value < node.getItem())
            node.setLeftChild(addNode(node.getLeftChild(), value));
        if (value > node.getItem())
            node.setRightChild(addNode(node.getRightChild(), value));
        return node;
    }

    private static void addChild(int[] array, int leftBound, int rightBound){
        if (rightBound < leftBound)
            return;
        if (leftBound == rightBound) {
            root = addNode(root, array[leftBound]);
            return;
        }
        int m = (leftBound + rightBound) / 2;
        root = addNode(root, m);
        addChild(array, leftBound, m-1);
        addChild(array, m+1, rightBound);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int N = cin.nextInt();
        int []array = new int[N];
        for (int i=0;i<N;++i)
            array[i] = i;
        addChild(array, 0, N-1);
        printTree();
    }
    
}
