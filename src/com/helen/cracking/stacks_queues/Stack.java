package com.helen.cracking.stacks_queues;

import com.helen.cracking.linkedlists.SingleLinkedList;

import java.util.Scanner;

public class Stack<T extends Comparable<T>> {

    public static class Node<T extends Comparable<T>> {

        T value;
        T min;

        Node<T> next;

        public Node(T value) {
            this.value = value;
            this.min = value;
            this.next = null;
        }

        public T getValue() {
            return value;
        }
    }

    private Node<T> top = null;

    public void push(T item) {
        Node<T> node = new Node<T>(item);
        node.next = top;
        if (top != null)
            node.min = item.compareTo(top.min)<0?item:top.min;
        top = node;
    }

    public T pop() {
        if (top == null)
            return null;
        T item = top.getValue();
        top = top.next;
        return item;
    }

    public T peek() {
        if (top == null)
            return null;
        return top.getValue();
    }

    public void printStack(boolean printSpace, boolean min) {
        Node<T> currentNode = this.top;
        while (currentNode != null) {
            System.out.print((min?currentNode.min:currentNode.getValue()).toString() + (printSpace ? " " : ""));
            currentNode = currentNode.next;
        }
        System.out.println();
    }

    public T getMin(){
        if (top == null)
            return null;
        return top.min;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        Stack<Integer> stack = new Stack();
        for (int i = 0; i < N; ++i) {
            String s = scanner.nextLine();
            if (s.length() == 0) {
                i--;
                continue;
            }
            if (s.charAt(0) == '-') {
                System.out.println("Pop: " + stack.pop());
            } else if (s.charAt(0) == 'p') {
                stack.printStack(true, false);
                stack.printStack(true, true);
            }
            else {
                stack.push(Integer.parseInt(s));
            }
        }
    }

}
