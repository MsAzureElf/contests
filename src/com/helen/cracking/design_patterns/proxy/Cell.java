package com.helen.cracking.design_patterns.proxy;

public abstract class Cell {

    public static final int CLOSED = 0;
    public static final int OPENED = 1;

    protected int status;
    protected int left, top;

    public Cell(int left, int top){
        this.left = left;
        this.top = top;
        this.status = Cell.CLOSED;
    }

    public void open(){
        System.out.println("cell " + top + " " + left + " is beeing opened");
        this.status = Cell.OPENED;
    }

    public int getLeft() {
        return left;
    }

    public int getTop() {
        return top;
    }

    public int getStatus() {
        return status;
    }

    public abstract int getPoints();

}
