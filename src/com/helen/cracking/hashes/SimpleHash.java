package com.helen.cracking.hashes;

/**
 * Created by Helen on 27.12.2017.
 */
public class SimpleHash {

    private static int HASH_BASE = 31;
    private static long[] powers = null;

    //

    private void precalculate_powers(){
        powers = new long[12];
        powers[0] = 1;
        for (int i = 1; i < 12; ++i) {
            powers[i] = HASH_BASE * powers[-1];
        }
    }

    private long getHash(String a) {
        long hash = 0;
        if (powers == null)
            precalculate_powers();
        for (int i = 0; i < a.length(); ++i) {
            hash += (a.charAt(i) - 'a' + 1) * powers[i];
        }
        return hash;
    }

}
