package com.helen.cracking.testing;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Created by Helen on 03.01.2018.
 */
@RunWith(JUnit4.class)
public class UtilsTest extends Assert {

    @Ignore
    @Test(expected = NullPointerException.class, timeout = 100)
    public void testNormal() throws Exception {
        int []arrayIn = {10, 12, 3, 5, -1, 9};
        int []arrayOut = {-1, 3, 5, 9, 10, 12};
        assertArrayEquals(new Utils().sort(arrayIn), arrayOut);
    }

}